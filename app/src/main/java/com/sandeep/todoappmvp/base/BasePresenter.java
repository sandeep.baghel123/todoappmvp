package com.sandeep.todoappmvp.base;

public interface BasePresenter {
    void onDestroy();
}
