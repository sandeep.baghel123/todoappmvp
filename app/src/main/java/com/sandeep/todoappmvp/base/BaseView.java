package com.sandeep.todoappmvp.base;

public interface BaseView<T> {
    void showError(String error);

    void setPresenter(T presenter);
}
