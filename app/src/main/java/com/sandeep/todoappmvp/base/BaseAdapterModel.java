package com.sandeep.todoappmvp.base;

public interface BaseAdapterModel {
    int getSize();
}
