package com.sandeep.todoappmvp.interfaces;

public interface OnRecyclerItemClickListener {
    void onItemClick(int position);
}
