package com.sandeep.todoappmvp.model;

import com.sandeep.todoappmvp.model.db.TodoListDBAdapter;

import java.util.ArrayList;

public class ModelImpl
        implements Model {
    private TodoListDBAdapter dbAdapter;

    public ModelImpl() {
        dbAdapter = TodoListDBAdapter.getInstance();
    }

    @Override
    public ArrayList<Todo> getAllTodos() {
        return dbAdapter.getAllTodos();
    }

    @Override
    public boolean addTodo(Todo todo) {
        return dbAdapter.insert(todo.getTodo(), todo.getPlace());
    }

    @Override
    public boolean removeTodo(int id) {
        return dbAdapter.delete(id);
    }

    @Override
    public boolean modifyTodo(int id, String todo, String place) {
        return dbAdapter.update(id, todo, place);
    }
}
