package com.sandeep.todoappmvp.model;

import java.util.ArrayList;

public interface Model {
    ArrayList<Todo> getAllTodos();

    boolean addTodo(Todo todo);

    boolean removeTodo(int id);

    boolean modifyTodo(int id, String todo, String place);
}
