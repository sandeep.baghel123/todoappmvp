package com.sandeep.todoappmvp.model.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sandeep.todoappmvp.model.Todo;
import com.sandeep.todoappmvp.utils.TodoApplication;

import java.util.ArrayList;

public class TodoListDBAdapter {
//    public static final String TAG = TodoListDBAdapter.class.getSimpleName();

    public static final String DATABASE_NAME = "todo.db";
    public static final int DATABASE_VERSION = 1;

    public static final String CREATE_TABLE_TODO = "CREATE TABLE "
            + TodoTable.TABLE_NAME + " ("
            + TodoTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TodoTable.COLUMN_TODO_NAME + " VARCHAR(100) NOT NULL,"
            + TodoTable.COLUMN_TODO_PLACE + " VARCHAR(100) NOT NULL);";

    private static final int NOTHING_CHANGED = 0;
    private static TodoListDBAdapter instance;
    private SQLiteDatabase database;

    public TodoListDBAdapter(Context context) {
        database = new TodoListDBHelper(context, DATABASE_NAME,
                null, DATABASE_VERSION).getWritableDatabase();
    }

    public static synchronized TodoListDBAdapter getInstance() {
        if (instance == null) {
            instance = new TodoListDBAdapter(TodoApplication.getContext());
        }
        return instance;
    }

    public boolean insert(String todoName, String todoPlace) {
        ContentValues values = new ContentValues();
        values.put(TodoTable.COLUMN_TODO_NAME, todoName);
        values.put(TodoTable.COLUMN_TODO_PLACE, todoPlace);
        return database.insert(TodoTable.TABLE_NAME, null, values) > NOTHING_CHANGED;
    }

    public boolean delete(int id) {
        String where = TodoTable._ID + "=" + id;
        return database.delete(TodoTable.TABLE_NAME, where, null) > NOTHING_CHANGED;
    }

    public boolean update(int id, String todoName, String todoPlace) {
        ContentValues values = new ContentValues();
        values.put(TodoTable.COLUMN_TODO_NAME, todoName);
        values.put(TodoTable.COLUMN_TODO_PLACE, todoPlace);
        String where = TodoTable._ID + "=" + id;
        return database.update(TodoTable.TABLE_NAME, values, where, null) > NOTHING_CHANGED;
    }

    public ArrayList<Todo> getAllTodos() {
        ArrayList<Todo> todos = new ArrayList<>();

        Cursor cursor = database.query(TodoTable.TABLE_NAME,
                null, null, null,
                null, null, null);

        if (cursor != null && cursor.getColumnCount() > 0) {
            while (cursor.moveToNext()) {
                Todo todo = new Todo();
                todo.setId(cursor.getInt(0));
                todo.setTodo(cursor.getString(1));
                todo.setPlace(cursor.getString(2));
                todos.add(todo);
            }
            cursor.close();
        }
        return todos;
    }

    public static class TodoTable implements BaseColumns {
        public static final String TABLE_NAME = "todo";
        public static final String _ID = "_ID";
        public static final String COLUMN_TODO_NAME = "todo_name";
        public static final String COLUMN_TODO_PLACE = "todo_place";
    }

    private static class TodoListDBHelper extends SQLiteOpenHelper {

        public TodoListDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(@NonNull SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_TODO);
        }

        @Override
        public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
            db.needUpgrade(newVersion);
        }
    }
}
