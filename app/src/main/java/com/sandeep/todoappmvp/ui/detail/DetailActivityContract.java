package com.sandeep.todoappmvp.ui.detail;

import android.content.Intent;

import com.sandeep.todoappmvp.base.BasePresenter;
import com.sandeep.todoappmvp.base.BaseView;
import com.sandeep.todoappmvp.model.Todo;

public interface DetailActivityContract {
    interface View extends BaseView<Presenter> {
        void onUILoaded(Intent intent);

        void updateUIOnRemove();

        void updateUIOnModify();
    }

    interface Presenter extends BasePresenter {
        void onRemoveClick(int id);

        void onModifyClick(Todo todo);

        void loadSelectedTodo(Intent intent);

        Todo getUpdatedTodo(int id);
    }
}
