package com.sandeep.todoappmvp.ui.home;

import com.sandeep.todoappmvp.base.BasePresenter;
import com.sandeep.todoappmvp.base.BaseView;
import com.sandeep.todoappmvp.model.Todo;

import java.util.ArrayList;

public interface MainActivityContract {
    interface View extends BaseView<Presenter> {
        void showAllTodos(ArrayList<Todo> todos);

        void showNoTodos();

        void updateViewOnAdd();

        void navigateToDetails(Todo todo, int position);
    }

    interface Presenter extends BasePresenter {
        void loadTodos();

        void onAddClick(Todo todo);

        void onItemClick(int position);
    }
}
