package com.sandeep.todoappmvp.ui.home;

import com.sandeep.todoappmvp.model.Model;
import com.sandeep.todoappmvp.model.ModelImpl;
import com.sandeep.todoappmvp.model.Todo;
import com.sandeep.todoappmvp.ui.home.adapter.AdapterContract;

public class MainPresenterImpl
        implements MainActivityContract.Presenter {
    private MainActivityContract.View view;
    private Model model;
    private AdapterContract.Model adapterModel;

    public MainPresenterImpl(MainActivityContract.View view, AdapterContract.Model adapterModel) {
        this.view = view;
        this.view.setPresenter(this);
        model = new ModelImpl();
        this.adapterModel = adapterModel;
    }

    @Override
    public void loadTodos() {
        adapterModel.clearList();
        if (model.getAllTodos().size() > 0) {
            view.showAllTodos(model.getAllTodos());
            adapterModel.addTodos(model.getAllTodos());
        } else {
            view.showNoTodos();
            adapterModel.clearList();
        }
    }

    @Override
    public void onAddClick(Todo todo) {
        if (model.addTodo(todo)) {
            view.updateViewOnAdd();
            view.showAllTodos(model.getAllTodos());
            adapterModel.addTodos(model.getAllTodos());
        } else {
            view.showError("Un-able to add Todo");
        }
    }

    @Override
    public void onItemClick(int position) {
        view.navigateToDetails(adapterModel.getTodo(position), position);
    }

    @Override
    public void onDestroy() {
        view = null;
        model = null;
        adapterModel = null;
    }
}

