package com.sandeep.todoappmvp.ui.detail;

import android.content.Intent;

import com.sandeep.todoappmvp.model.Model;
import com.sandeep.todoappmvp.model.ModelImpl;
import com.sandeep.todoappmvp.model.Todo;

public class DetailActivityPresenterImpl
        implements DetailActivityContract.Presenter {
    private Model model;
    private DetailActivityContract.View view;

    public DetailActivityPresenterImpl(DetailActivityContract.View view) {
        this.view = view;
        this.view.setPresenter(this);
        model = new ModelImpl();
    }

    @Override
    public void onRemoveClick(int id) {
        if (model.removeTodo(id)) {
            view.updateUIOnRemove();
        } else {
            view.showError("No such Task ID exist!!");
        }
    }

    @Override
    public void onModifyClick(Todo todo) {
        if (model.modifyTodo(todo.getId(), todo.getTodo(), todo.getPlace())) {
            view.updateUIOnModify();
        } else {
            view.showError("Cannot modify!!");
        }
    }

    @Override
    public void loadSelectedTodo(Intent intent) {
        view.onUILoaded(intent);
    }

    @Override
    public Todo getUpdatedTodo(int id) {
        return model.getAllTodos().get(id);
    }

    @Override
    public void onDestroy() {
        view = null;
        model = null;
    }
}
