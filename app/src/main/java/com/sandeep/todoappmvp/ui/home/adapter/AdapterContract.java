package com.sandeep.todoappmvp.ui.home.adapter;

import com.sandeep.todoappmvp.base.BaseAdapterModel;
import com.sandeep.todoappmvp.base.BaseAdapterView;
import com.sandeep.todoappmvp.model.Todo;

import java.util.ArrayList;

public interface AdapterContract {
    interface View extends BaseAdapterView {

    }

    interface Model extends BaseAdapterModel {
        void addTodos(ArrayList<Todo> todos);

        Todo getTodo(int position);

        void removeTodo(int position);

        void clearList();
    }
}
