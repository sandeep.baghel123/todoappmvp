package com.sandeep.todoappmvp.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sandeep.todoappmvp.R;
import com.sandeep.todoappmvp.interfaces.OnRecyclerItemClickListener;
import com.sandeep.todoappmvp.model.Todo;
import com.sandeep.todoappmvp.ui.detail.DetailActivity;
import com.sandeep.todoappmvp.ui.home.adapter.TodoAdapter;
import com.sandeep.todoappmvp.utils.AppConstants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements MainActivityContract.View,
        View.OnClickListener {
    private MainActivityContract.Presenter presenter;
    private AppCompatEditText etTodo, etTodoPlace;
    private AppCompatTextView tvNoTodos;
    private RecyclerView rvTodos;
    private TodoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etTodo = findViewById(R.id.et_todo);
        etTodoPlace = findViewById(R.id.et_todo_place);
        rvTodos = findViewById(R.id.rv_todos);
        tvNoTodos = findViewById(R.id.tv_no_todos);

        findViewById(R.id.btn_add).setOnClickListener(this);

        rvTodos.setLayoutManager(new LinearLayoutManager(this));

        adapter = new TodoAdapter(this);
        rvTodos.setAdapter(adapter);

        adapter.setClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onItemClick(int position) {
                presenter.onItemClick(position);
            }
        });

        presenter = new MainPresenterImpl(this, adapter);
        presenter.loadTodos();
    }

    @Override
    public void showAllTodos(ArrayList<Todo> todos) {
        rvTodos.setVisibility(View.VISIBLE);
        tvNoTodos.setVisibility(View.GONE);
    }

    @Override
    public void showNoTodos() {
        rvTodos.setVisibility(View.GONE);
        tvNoTodos.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateViewOnAdd() {
        clearAllFields();
    }

    @Override
    public void navigateToDetails(Todo todo, int position) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(AppConstants.TODO_DATA, todo);
        intent.putExtra(AppConstants.TODO_POSITION, position);
        startActivity(intent);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(MainActivityContract.Presenter presenter) {
        this.presenter = presenter;
    }

    private void clearAllFields() {
        etTodo.setText("");
        etTodoPlace.setText("");
    }

    @Override
    public void onClick(@NonNull View v) {
        if (v.getId() == R.id.btn_add) {
            Todo todo = new Todo();
            if (etTodo.getText() != null && etTodoPlace.getText() != null) {
                String todoName = etTodo.getText().toString();
                String todoPlace = etTodoPlace.getText().toString();
                if (TextUtils.isDigitsOnly(todoName)) {
                    Toast.makeText(this, "Please enter valid Todo Name!!", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(todoPlace)) {
                    Toast.makeText(this, "Please enter valid Todo Place!!", Toast.LENGTH_SHORT).show();
                } else {
                    todo.setTodo(todoName);
                    todo.setPlace(todoPlace);
                    presenter.onAddClick(todo);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
