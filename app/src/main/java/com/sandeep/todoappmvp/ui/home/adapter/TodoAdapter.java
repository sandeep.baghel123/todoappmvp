package com.sandeep.todoappmvp.ui.home.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sandeep.todoappmvp.R;
import com.sandeep.todoappmvp.interfaces.OnRecyclerItemClickListener;
import com.sandeep.todoappmvp.model.Todo;

import java.util.ArrayList;
import java.util.Locale;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.ViewHolder>
        implements AdapterContract.View, AdapterContract.Model {
    private static final String TAG = TodoAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<Todo> todos;
    private OnRecyclerItemClickListener clickListener;

    public TodoAdapter(Context context) {
        this.context = context;
        todos = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_todo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.e(TAG, "onBindViewHolder: Todo, ID: " + todos.get(position).getId() +
                "Name: " + todos.get(position).getTodo() +
                "Place: " + todos.get(position).getPlace());
        holder.id.setText(String.format(Locale.ENGLISH, "%d", todos.get(position).getId()));
        holder.todoName.setText(todos.get(position).getTodo());
        holder.todoPlace.setText(todos.get(position).getPlace());
    }

    @Override
    public int getItemCount() {
        return getSize();
    }

    @Override
    public int getSize() {
        return todos.size();
    }

    @Override
    public void reload() {
        notifyDataSetChanged();
    }

    @Override
    public void addTodos(ArrayList<Todo> todos) {
        clearList();
        this.todos.addAll(todos);
        reload();
    }

    @Override
    public Todo getTodo(int position) {
        return todos.get(position);
    }

    @Override
    public void removeTodo(int position) {
        todos.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void clearList() {
        todos.clear();
        reload();
    }

    public void setClickListener(OnRecyclerItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView id;
        public AppCompatTextView todoName;
        public AppCompatTextView todoPlace;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.tv_todo_ids);
            todoName = itemView.findViewById(R.id.tv_todo_name);
            todoPlace = itemView.findViewById(R.id.tv_todo_place);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
