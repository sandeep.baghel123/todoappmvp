package com.sandeep.todoappmvp.ui.detail;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.sandeep.todoappmvp.R;
import com.sandeep.todoappmvp.model.Todo;
import com.sandeep.todoappmvp.utils.AppConstants;

import java.util.Locale;

public class DetailActivity extends AppCompatActivity
        implements DetailActivityContract.View {
    private AppCompatEditText etTodoNameModify, etTodoPlaceModify;
    private AppCompatTextView tvTodoId, tvTodoName, tvTodoPlace, tvTodoRemoved;
    private LinearLayout llTodo;
    private int selectedTodoPosition;
    private DetailActivityContract.Presenter presenter;
    private Todo todo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        etTodoNameModify = findViewById(R.id.et_todo_name_modify);
        etTodoPlaceModify = findViewById(R.id.et_todo_place_modify);

        llTodo = findViewById(R.id.ll_todo);

        tvTodoId = findViewById(R.id.tv_todo_ids);
        tvTodoName = findViewById(R.id.tv_todo_name);
        tvTodoPlace = findViewById(R.id.tv_todo_place);
        tvTodoRemoved = findViewById(R.id.tv_todo_removed);

        AppCompatButton btnRemove = findViewById(R.id.btn_remove);
        AppCompatButton btnModify = findViewById(R.id.btn_modify);

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onRemoveClick(todo.getId());

            }
        });
        btnModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etTodoNameModify.getText().toString();
                String place = etTodoPlaceModify.getText().toString();
                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(DetailActivity.this, "Please enter name to modify!!", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(place)) {
                    Toast.makeText(DetailActivity.this, "Please enter place to modify!!", Toast.LENGTH_SHORT).show();
                } else {
                    Todo todoNew = new Todo();
                    todoNew.setId(todo.getId());
                    todoNew.setTodo(name);
                    todoNew.setPlace(place);
                    presenter.onModifyClick(todoNew);
                }
            }
        });

        presenter = new DetailActivityPresenterImpl(this);
        presenter.loadSelectedTodo(getIntent());
    }

    @Override
    public void onUILoaded(Intent intent) {
        if (intent != null) {
            todo = (Todo) intent.getSerializableExtra(AppConstants.TODO_DATA);
            selectedTodoPosition = intent.getIntExtra(AppConstants.TODO_POSITION, 0);
            if (todo != null) {
                String todoId = String.valueOf(todo.getId());
                String todoName = todo.getTodo();
                String todoPlace = todo.getPlace();

                tvTodoId.setText(todoId);
                tvTodoName.setText(todoName);
                tvTodoPlace.setText(todoPlace);
            } else {
                tvTodoRemoved.setVisibility(View.VISIBLE);
                llTodo.setVisibility(View.GONE);
            }
        } else {
            tvTodoRemoved.setVisibility(View.VISIBLE);
            llTodo.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateUIOnRemove() {
        llTodo.setVisibility(View.GONE);
        tvTodoRemoved.setVisibility(View.VISIBLE);
        onBackPressed();
    }

    @Override
    public void updateUIOnModify() {
        clearAllFields();
        todo = presenter.getUpdatedTodo(selectedTodoPosition);
        tvTodoId.setText(String.format(Locale.ENGLISH, "%d", todo.getId()));
        tvTodoName.setText(todo.getTodo());
        tvTodoPlace.setText(todo.getPlace());
    }

    private void clearAllFields() {
        etTodoNameModify.setText("");
        etTodoPlaceModify.setText("");
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(DetailActivityContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
