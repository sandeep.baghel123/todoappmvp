package com.sandeep.todoappmvp.utils;

public final class AppConstants {
    public static final String TODO_DATA = "TODO";
    public static final String TODO_POSITION = "TODO_POSITION";
}
